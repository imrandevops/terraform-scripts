# Creating an aws_ec2_instance 

resource "aws_instance" "web" {
  count                  = "1"
  ami                    = "ami-00a9d4a05375b2763"
  instance_type          = "t2.micro"
  user_data              = "${file("scripts/apache.sh")}"
  key_name               = "key-web"
}

resource "aws_key_pair" "web" {
  key_name   = "key-web"
  public_key = "${file("scripts/web.pub")}"
}
